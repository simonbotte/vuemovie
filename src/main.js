import Vue from "vue";
import App from "./App.vue";

import VueRouter from "vue-router";

import movies from "./components/movies.vue";
import editMovie from "./components/editMovie.vue";
import addMovie from "./components/addMovie.vue";
import movie from "./components/movie.vue";

const routes = [
    { path: "/", component: movies, name: "home" },
    { path: "/movie/:idMovie/edit", name: "edit", component: editMovie },
    { path: "/movie/:idMovie", name: "movie", component: movie },
    { path: "/add", name: "add", component: addMovie },
];

const router = new VueRouter({
    routes,
});

Vue.use(VueRouter);
Vue.config.productionTip = false;

//variable partagées
window.shared_data = {
    movies: [
        {
            id: 1,
            title: "Interstellar",

            director: {
                name: "Christopher Nolan",
                nationality: "Américan",
                birthDate: "30/07/1970",
            },
            plot: "Un film qui parle de physique et d'amour",
            poster: null,
            runtime: null,
            releaseDate: null,
            langue: null,
            genre: null,

            note: null,
        },
        {
            id: 2,
            title: "Tenet",

            director: {
                name: "Christopher Nolan",
                nationality: "Américan",
                birthDate: "30/07/1970",
            },
            plot: "Un film qui parle de temps",
            poster: null,
            runtime: null,
            releaseDate: null,
            langue: null,
            genre: null,

            note: null,
        },
        {
            id: 3,
            title: "Palmer",

            director: {
                name: "Fisher Stevens",
                nationality: "Américan",
                birthDate: "27/09/1963",
            },
            plot:
                "Après 12 ans en prison, l’ex-prodige du football Eddie Palmer revient chez lui pour repartir de zéro. Il se lie d’amitié avec un jeune garçon malmené par la vie, mais son passé trouble menace de réduire en cendres ce nouveau départ.",
            poster: null,
            runtime: null,
            releaseDate: null,
            langue: null,
            genre: null,

            note: null,
        },
        {
            id: 4,
            title: "Avengers: Infinity war",

            director: {
                name: "Joe Russo, Anthony Russo",
                nationality: "Américan",
                birthDate: "03/02/1970, 19/07/1971",
            },
            plot: "Laser, pierre, gants",
            poster: null,
            runtime: null,
            releaseDate: null,
            langue: null,
            genre: null,

            note: null,
        },
        {
            id: 5,
            title: "Avengers: Endgame",

            director: {
                name: "Joe Russo, Anthony Russo",
                nationality: "Américan",
                birthDate: "03/02/1970, 19/07/1971",
            },
            plot: "Laser, pierre, gants",
            poster: null,
            runtime: null,
            releaseDate: null,
            langue: null,
            genre: null,

            note: null,
        },
    ],
    aMovie: null,
    searchText: "",
    omdbApiKey: "3dadc645",
};

new Vue({
    router: router,
    render: (h) => h(App),
}).$mount("#app");

window.addEventListener("load", () => {
    window.shared_data.movies.forEach((movie) => {
        let url =
            "http://www.omdbapi.com/?apikey=" +
            window.shared_data.omdbApiKey +
            "&t=" +
            movie.title;

        fetch(url)
            .then((response) => response.json())
            .then((data) => {
                movie.poster = data.Poster;
                movie.runtime = data.Runtime;
                movie.releaseDate = data.Year;
                movie.langue = data.Language;
                movie.genre = data.Genre;
            });
    });
});
